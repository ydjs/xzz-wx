import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

//vant-ui
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

//style
import './style/common.scss'
import './assets/css/font-awesome.min.css'

//axios
import axios from 'axios'
import './assets/js/http.js'
Vue.prototype.$axios=axios

//全局方法
import global from './assets/js/global.js'
Vue.prototype.common = global

//全局组件
import cmsComponents from './components/index.js'
Vue.use(cmsComponents)

//vue-awesome-swiper
import 'swiper/dist/css/swiper.css'

//rem
import './assets/js/rem.js'
//Vue.use(rem);

//解手机点击延迟
import FastClick from 'fastclick'
FastClick.attach(document.body);

//登录拦截
router.beforeEach((to, from, next) => {
	//保存openId
	if(global.getParamString('openid')){
		console.log('openId:' + global.getParamString('openid'));
		global.setCookie('openId',global.getParamString('openid'));
	}
	let myCookie = global.getCookie('token');
    if (!myCookie && to.path != '/' && to.path != '/login' && to.path != '/eyeReport'){
		next({
			path:'/login'
		})
	}
	else{
		next();
	}
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
